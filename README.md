Тестовый модуль
===============
Описание

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist amd-php-dev/module_test "*"
```

or add

```
"amd-php-dev/module_test": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \amd_php_dev\module_test\AutoloadExample::widget(); ?>```