<?php

namespace amd_php_dev\module_test;

/**
 * test module definition class
 */
class Module extends \amd_php_dev\yii2_components\modules\Module
{
    //public $layout      = '@app/views/layouts/default';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'amd_php_dev\module_test\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        //$this->modules = [
        //
        //];

        // custom initialization code goes here
    }

    //public static function getMenuItems() {
    //    return [
    //        'section' => 'test',
    //        'items' => [
    //            [
    //                'label' => 'test',
    //                'items' => [
    //                    ['label' => 'label', 'url' => ['']],
    //                ]
    //            ]
    //        ],
    //    ];
    //}
}
